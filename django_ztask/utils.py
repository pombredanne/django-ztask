# -*- coding: utf-8 -*-

from django.conf import settings

def setting(name, default=''): 
    return getattr(settings, name, default)


