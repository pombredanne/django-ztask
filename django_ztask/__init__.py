'''Django ZTask'''

import os

VERSION = (0, 1, 7)

__version__ = '.'.join(map(str, VERSION[0:3])) + ''.join(VERSION[3:])
__author__ = 'Jason Allum and Dave Martorana'
__contact__ = 'jason@dmgctrl.com'
__homepage__ = 'http://github.com/dmgctrl/django-ztask'
__docformat__ = 'markdown'
__license__ = 'BSD (3 clause)'

# example settings
#ZTASKD_URL = getattr(settings, 'ZTASKD_URL', 'tcp://127.0.0.1:5555')
#ZTASKD_ALWAYS_EAGER = getattr(settings, 'ZTASKD_ALWAYS_EAGER', False)
#ZTASKD_DISABLED = getattr(settings, 'ZTASKD_DISABLED', False)
#ZTASKD_RETRY_COUNT = getattr(settings, 'ZTASKD_RETRY_COUNT', 5)
#ZTASKD_RETRY_AFTER = getattr(settings, 'ZTASKD_RETRY_AFTER', 5)
#ZTASKD_LOGGER = getattr(settings, 'ZTASKD_LOGGER', 'django_ztaskd')
#ZTASKD_ON_LOAD = getattr(settings, 'ZTASKD_ON_LOAD', ())
#ZTASKD_ON_CALL_COMPLETE = getattr(settings, 'ZTASKD_ON_COMPLETE', ())
