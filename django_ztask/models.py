# -*- coding: utf-8 -*-

import uuid
from datetime import datetime

from django.db import models


class QuerySetManager(models.Manager):
    
    def __getattr__(self, attr, *args):
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            return getattr(self.get_query_set(), attr, *args)
    
    def get_query_set(self):
        return self.model.QuerySet(self.model)
    

class Task(models.Model):
    
    uuid = models.CharField(max_length=36, primary_key=True)
    function_name = models.CharField(max_length=255)
    args = models.TextField()
    kwargs = models.TextField()
    retry_count = models.IntegerField(default=0)
    last_exception = models.TextField(blank=True, null=True)
    next_attempt = models.FloatField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    failed = models.DateTimeField(blank=True, null=True)
    
    def save(self, *args, **kwargs):
        if not self.uuid:
            self.created = datetime.utcnow()
            self.uuid = uuid.uuid4()
        super(Task, self).save(*args, **kwargs)
    
    class Meta:
        db_table = 'django_ztask_task'
    
